package firstPart;
import java.util.ArrayList;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        MarsRover mars = new MarsRover();
        System.out.println("Please, write the block number: ");
        Scanner scan = new Scanner(System.in);
        System.out.println("Minimum number of commands: " + minNumberOfCommands(mars, scan.nextInt()));
    }
    private static int minNumberOfCommands(MarsRover mars, int blockNumber) {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        int count = 0;
        int speed = 1;
        int steps = 0;
        while(speed < blockNumber){
            speed *= 2;
            arr.add(speed-1);
            steps++;
        }
        int lastElement = (int)arr.get(steps-1);
        if(steps > 4 && blockNumber > lastElement-speed/2) {
            int ded = 3;
            int s = 4;
            steps -= 5;
            ArrayList<Integer> list = new ArrayList<>();
            list.add(lastElement-ded);
            while (steps>0){
                ded += s;
                s *= 2;
                steps -= 1;
                list.add(lastElement-ded);
            }
            if(list.contains(blockNumber+1)){
                mars.R();
                mars.A();
                mars.R();
                count += 3;
            }
            if(list.contains(blockNumber+2)){
                mars.A();
                mars.R();
                mars.A();
                mars.A();
                mars.R();
                count += 5;
            }
        }
        while (blockNumber > (mars.getPosition()+ mars.getSpeed()/2)) {
            mars.A();
            count++;
        }
        int sp = mars.getSpeed();
        while (blockNumber != mars.getPosition()){
            if(blockNumber > mars.getPosition() && mars.getSpeed() > 0){
                mars.R();
                int dif = blockNumber-mars.getPosition();
                if(!arr.contains(dif) && (dif != sp/2 || dif < 3)){
                    mars.A();
                    count++;
                    if(dif % 2 == 0 && dif/2 == 2){
                        mars.A();
                        count++;
                    }
                }
                mars.R();
                count += 2; // за 2 вызова R()
                while (blockNumber > (mars.getPosition()+ mars.getSpeed()/2)) {
                    mars.A();
                    count++;
                }
            }else {
                mars.R();
                count++;
                if(blockNumber < mars.getPosition()){
                    while (blockNumber < mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }else{
                    while (blockNumber > mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }
            }
        }
        return count;
    }
}