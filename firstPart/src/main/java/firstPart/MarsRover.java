package firstPart;
public class MarsRover {
    private int speed; //скорость
    private int position; //номер блока
    public void setSpeed(int speed) { //сеттеры геттеры
        this.speed = speed;
    }
    public int getSpeed() {
        return speed;
    }
    public void setPosition(int position) {
        this.position = position;
    }
    public int getPosition() {
        return position;
    }
    public MarsRover(){ //конструктор
        speed = 1;
        position = 0;
    }
    public void A(){ //ускорение
        position += speed;
        speed *= 2;
    }
    public void R(){ //обратный ход
         if(speed > 0){
            speed = -1;
        } else {
            speed = 1;
        }
    }
}