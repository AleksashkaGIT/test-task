package firstPart;
import firstPart.*;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class MarsRoverTest {
	static MarsRover mars;
	@BeforeAll
    static void initAll(){
        mars = new MarsRover();
    }
	@AfterEach
	void returnFields(){
		mars.setPosition(0);
		mars.setSpeed(1);
	}
	@Test
    void initSpeed() {
        assertEquals(mars.getSpeed(), 1);
    }
	@Test
    void initPosition() {
        assertTrue(mars.getPosition() == 0);
    }
	@Test
    void moveAheadPosition() {
		mars.A();
		assertEquals(mars.getPosition(), 1);
    }
	@Test
    void moveAheadSpeed() {
		mars.A();
        assertEquals(mars.getSpeed(), 2);
    }
	@Test
    void reverse() {
		mars.R();
        assertTrue(mars.getSpeed() == -1);
    }
	@Test
    void moveAAR() {
		mars.A();
		mars.A();
		mars.R();
        assertTrue(mars.getSpeed() == -1);
		assertTrue(mars.getPosition() == 3);
    }
	@Test
    void minNumberOfCommandsTest3() {
		int count = 0; 
		while (3 > (mars.getPosition()+ mars.getSpeed()/2)) { //на входе число 3
            mars.A();
            count++;
        }
        assertTrue(count == 2);
    }
	@Test
    void minNumberOfCommandsTest6() {  //на входе число 6
		ArrayList<Integer> arr = new ArrayList<Integer>();
        int count = 0;
        int speed = 1;
        int steps = 0;
        while(speed < 6){
            speed *= 2;
            arr.add(speed-1);
            steps++;
        }
        while (6 > (mars.getPosition()+ mars.getSpeed()/2)) {
            mars.A();
            count++;
        }
        int sp = mars.getSpeed();
        while (6 != mars.getPosition()){
            if(6 > mars.getPosition() && mars.getSpeed() > 0){
                mars.R();
                int dif = 6-mars.getPosition();
                if(!arr.contains(dif) && (dif != sp/2 || dif < 3)){
                    mars.A();
                    count++;
                    if(dif % 2 == 0 && dif/2 == 2){
                        mars.A();
                        count++;
                    }
                }
                mars.R();
                count += 2; // за 2 вызова R()
                while (6 > (mars.getPosition()+ mars.getSpeed()/2)) {
                    mars.A();
                    count++;
                }
            }else {
                mars.R();
                count++;
                if(6 < mars.getPosition()){
                    while (6 < mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }else{
                    while (6 > mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }
            }
        }
        assertTrue(count == 5);
    }
	@Test
    void minNumberOfCommandsTest4() {  //на входе число 4
		ArrayList<Integer> arr = new ArrayList<Integer>();
        int count = 0;
        int speed = 1;
        int steps = 0;
        while(speed < 4){
            speed *= 2;
            arr.add(speed-1);
            steps++;
        }
        while (4 > (mars.getPosition()+ mars.getSpeed()/2)) {
            mars.A();
            count++;
        }
        int sp = mars.getSpeed();
        while (4 != mars.getPosition()){
            if(4 > mars.getPosition() && mars.getSpeed() > 0){
                mars.R();
                int dif = 4-mars.getPosition();
                if(!arr.contains(dif) && (dif != sp/2 || dif < 3)){
                    mars.A();
                    count++;
                    if(dif % 2 == 0 && dif/2 == 2){
                        mars.A();
                        count++;
                    }
                }
                mars.R();
                count += 2; // за 2 вызова R()
                while (4 > (mars.getPosition()+ mars.getSpeed()/2)) {
                    mars.A();
                    count++;
                }
            }else {
                mars.R();
                count++;
                if(4 < mars.getPosition()){
                    while (4 < mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }else{
                    while (4 > mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }
            }
        }
        assertTrue(count == 5);
    }
	@Test
    void minNumberOfCommandsTest5() { //на входе число 5
        ArrayList<Integer> arr = new ArrayList<Integer>();
        int count = 0;
        int speed = 1;
        int steps = 0;
        while(speed < 5){
            speed *= 2;
            arr.add(speed-1);
            steps++;
        }
        while (5 > (mars.getPosition()+ mars.getSpeed()/2)) {
            mars.A();
            count++;
        }
        int sp = mars.getSpeed();
        while (5 != mars.getPosition()){
            if(5 > mars.getPosition() && mars.getSpeed() > 0){
                mars.R();
                int dif = 5-mars.getPosition();
                if(!arr.contains(dif) && (dif != sp/2 || dif < 3)){
                    mars.A();
                    count++;
                    if(dif % 2 == 0 && dif/2 == 2){
                        mars.A();
                        count++;
                    }
                }
                mars.R();
                count += 2; // за 2 вызова R()
                while (5 > (mars.getPosition()+ mars.getSpeed()/2)) {
                    mars.A();
                    count++;
                }
            }else {
                mars.R();
                count++;
                if(5 < mars.getPosition()){
                    while (5 < mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }else{
                    while (5 > mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }
            }
        }
        assertTrue(count == 7);
    }
	@Test
    void minNumberOfCommandsTest20() {  //на входе число 20
		 ArrayList<Integer> arr = new ArrayList<Integer>();
        int count = 0;
        int speed = 1;
        int steps = 0;
        while(speed < 20){
            speed *= 2;
            arr.add(speed-1);
            steps++;
        }
        int lastElement = (int)arr.get(steps-1);
        if(steps > 4 && 20 > lastElement-speed/2) {
            int ded = 3;
            int s = 4;
            steps -= 5;
            ArrayList<Integer> list = new ArrayList<>();
            list.add(lastElement-ded);
            while (steps>0){
                ded += s;
                s *= 2;
                steps -= 1;
                list.add(lastElement-ded);
            }
            if(list.contains(21)){
                mars.R();
                mars.A();
                mars.R();
                count += 3;
            }
            if(list.contains(22)){
                mars.A();
                mars.R();
                mars.A();
                mars.A();
                mars.R();
                count += 5;
            }
        }
        while (20 > (mars.getPosition()+ mars.getSpeed()/2)) {
            mars.A();
            count++;
        }
        int sp = mars.getSpeed();
        while (20 != mars.getPosition()){
            if(20 > mars.getPosition() && mars.getSpeed() > 0){
                mars.R();
                int dif = 20-mars.getPosition();
                if(!arr.contains(dif) && (dif != sp/2 || dif < 3)){
                    mars.A();
                    count++;
                    if(dif % 2 == 0 && dif/2 == 2){
                        mars.A();
                        count++;
                    }
                }
                mars.R();
                count += 2; // за 2 вызова R()
                while (20 > (mars.getPosition()+ mars.getSpeed()/2)) {
                    mars.A();
                    count++;
                }
            }else {
                mars.R();
                count++;
                if(20 < mars.getPosition()){
                    while (20 < mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }else{
                    while (20 > mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }
            }
        }
        assertTrue(count == 12);
    }
	@Test
    void minNumberOfCommandsTest26() {  //на входе число 26
		ArrayList<Integer> arr = new ArrayList<Integer>();
        int count = 0;
        int speed = 1;
        int steps = 0;
        while(speed < 26){
            speed *= 2;
            arr.add(speed-1);
            steps++;
        }
        int lastElement = (int)arr.get(steps-1);
        if(steps > 4 && 26 > lastElement-speed/2) {
            int ded = 3;
            int s = 4;
            steps -= 5;
            ArrayList<Integer> list = new ArrayList<>();
            list.add(lastElement-ded);
            while (steps>0){
                ded += s;
                s *= 2;
                steps -= 1;
                list.add(lastElement-ded);
            }
            if(list.contains(27)){
                mars.R();
                mars.A();
                mars.R();
                count += 3;
            }
            if(list.contains(28)){
                mars.A();
                mars.R();
                mars.A();
                mars.A();
                mars.R();
                count += 5;
            }
        }
        while (26 > (mars.getPosition()+ mars.getSpeed()/2)) {
            mars.A();
            count++;
        }
        int sp = mars.getSpeed();
        while (26 != mars.getPosition()){
            if(26 > mars.getPosition() && mars.getSpeed() > 0){
                mars.R();
                int dif = 26-mars.getPosition();
                if(!arr.contains(dif) && (dif != sp/2 || dif < 3)){
                    mars.A();
                    count++;
                    if(dif % 2 == 0 && dif/2 == 2){
                        mars.A();
                        count++;
                    }
                }
                mars.R();
                count += 2; // за 2 вызова R()
                while (26 > (mars.getPosition()+ mars.getSpeed()/2)) {
                    mars.A();
                    count++;
                }
            }else {
                mars.R();
                count++;
                if(26 < mars.getPosition()){
                    while (26 < mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }else{
                    while (26 > mars.getPosition()){
                        mars.A();
                        count++;
                    }
                }
            }
        }
        assertTrue(count == 13);
    }
}